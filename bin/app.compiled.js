#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _nodeRedisPubsub = require('node-redis-pubsub');

var _nodeRedisPubsub2 = _interopRequireDefault(_nodeRedisPubsub);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _child_process = require('child_process');

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).usage('<configurations..>').parse(process.argv);

var config = { port: 6379, scope: 'test' },
    nrp = new _nodeRedisPubsub2['default'](config);

var key = _crypto2['default'].randomBytes(32).toString('hex');

var getFeedr = function getFeedr(callback, opts) {
  opts = opts || {};
  return function (data) {
    var options = [data.url];
    if (opts.hash == true) options.push('--hash-only');
    var feeds = run('feedr', options, function (result) {
      var lines = result.split(/(\r?\n)/g);
      lines.forEach(callback);
    });
  };
};

var run = function run(cmd, options, callback) {
  var command = (0, _child_process.spawn)(cmd, options);
  command.stdout.setEncoding('utf8');

  var result = '';
  command.stdout.on('data', function (data) {
    result += data.toString();
  });

  command.on('close', function (code) {
    return callback(result);
  });
};

var callPostr = function callPostr(callback) {
  return function (result) {
    try {
      var obj = JSON.parse(result);
      return callback(obj);
    } catch (e) {
      return;
    }
  };
};

var checkExists = function checkExists(obj) {
  if (obj.link) {
    nrp.emit('postr:exists', { feed: obj.feed, link: obj.link, hash: obj.hash });
  }
};

var postInsert = function postInsert(obj) {
  if (obj.feed && obj.link && obj.hash) {
    nrp.emit('postr:insert', obj);
  }
};

var register = function register() {
  nrp.emit('castr:add:scrapr', { id: key });
};

nrp.on('scrapr:isUpdated:' + key, getFeedr(callPostr(checkExists), { hash: true }));
nrp.on('scrapr:scrap:' + key, getFeedr(callPostr(postInsert)));

nrp.on('castr:callback', register);

process.on('SIGINT', function () {
  console.log("\nBye!");
  nrp.emit('castr:remove:scrapr', { id: key });
  nrp.quit();
  process.exit();
});

register();
console.log('Listen scrapr:* as ' + key);
