#!/usr/bin/env node
import program from 'commander'
import NRP from 'node-redis-pubsub'
import crypto from 'crypto'
import {spawn} from 'child_process'

import pkg from '../package.json'

program
  .version(pkg.version)
  .usage('<configurations..>')
  .parse(process.argv)

const config = { port: 6379, scope: 'test' },
      nrp = new NRP(config)

const key = crypto.randomBytes(32).toString('hex')

const getFeedr = (callback, opts) => {
  opts = opts || {}
  return (data) => {
    let options = [data.url]
    if (opts.hash == true) options.push('--hash-only')
    let feeds = run('feedr', options, (result) => {
      var lines = result.split(/(\r?\n)/g)
      lines.forEach(callback)
    })
  }
}

const run = (cmd, options, callback) => {
    let command = spawn(cmd, options)
    command.stdout.setEncoding('utf8')

    var result = ''
    command.stdout.on('data', (data) => {
         result += data.toString()
    })

    command.on('close', (code) => {
        return callback(result)
    })
}

const callPostr = (callback) => {
  return (result) => {
    try {
      var obj = JSON.parse(result)
      return callback(obj)
    } catch (e) {
      return;
    }
  }
}

const checkExists = (obj) => {
  if (obj.link) {
    nrp.emit('postr:exists', {feed: obj.feed, link: obj.link, hash: obj.hash})
  }
}

const postInsert = (obj) => {
  if (obj.feed && obj.link && obj.hash) {
    nrp.emit('postr:insert', obj)
  }
}

const register = () => {
  nrp.emit('castr:add:scrapr', {id: key})
}

nrp.on(`scrapr:isUpdated:${key}`, getFeedr(callPostr(checkExists), {hash: true}))
nrp.on(`scrapr:scrap:${key}`, getFeedr(callPostr(postInsert)))

nrp.on('castr:callback', register)

process.on('SIGINT', () => {
    console.log("\nBye!")
    nrp.emit('castr:remove:scrapr', {id: key})
    nrp.quit()
    process.exit()
})

register()
console.log(`Listen scrapr:* as ${key}`)
